Hitman player;
Enemy enemy;

ArrayList<Enemy> enemies = new ArrayList<Enemy>();
int currentLevel = 1;
PImage img;
PFont font;
 
boolean forwardKeyPressed = false;
boolean backwardKeyPressed = false;

boolean gameOver = false;

// Bullet variables
int bulletCount = 10;
ArrayList<Bullet> bullets = new ArrayList<Bullet>();
int bulletIndex = 0;


void setup()
{
  size(800, 800, P2D);
  img = loadImage("floor.jpg");
  font = loadFont("JoystixMonospace-Regular-48.vlw");
  textureMode(NORMAL);
  
  gameOver = false;
    
  player = new Hitman();
  
  enemies = new ArrayList<Enemy>();
   
  for(int i = 0; i < currentLevel;i++)
  {      
   
    enemy = new Enemy(currentLevel);
    enemy.directionX = 0;
    enemy.directionY = -1;
    
    enemy.x = random(100, 800);
    enemy.y = random(100, 800);
    
    enemies.add(enemy);
  }
  
  //Spawn position. 
  player.x = 50;
  player.y = 50;
   
  // Set the initial direction to be down. Directions should always be of 1 length.
  // Here it is 1 length because we are just pointing down 1 in the y.
  player.directionX = -1;
  player.directionY = 1;
   
  //Playes Speed
  player.speed = 3;

   
  for (int i = 0; i < bulletCount; i += 1)
  {
    Bullet bullet = new Bullet();
    bullet.x = -1000;
    bullet.y = -1000;
    bullets.add(bullet);
  }
  
  
}
 
void draw()
{
  // Clear the screen
  background(100);
  int r=4;
  noStroke();
  textureWrap(REPEAT);
  beginShape();
  texture(img);
  vertex(0,0,0,0);
  vertex(width,0,r,0);
  vertex(width,height,r,r);
  vertex(0,height,0,r);
  endShape();  
  
  textFont(font);
  textSize(32);
  fill(255);
  text("SCORE: "+(currentLevel-1), 10, 30 ); 
  
  if(gameOver)
  {
      textFont(font);
      textSize(50);
      fill(255);
      text("GAME OVER" , width/2-200 , height/2);
      textSize(24);
      text("Press SpaceBar to Play Again" , width/2-290 , height/2+50);
        
  }

  player.lookAt(mouseX, mouseY);
  player.draw();
  
  float speedFactor = (currentLevel)*8; 
  
  for(int i = 0; i < enemies.size(); i++)
  {
    enemies.get(i).lookAt(player.x, player.y);
    enemies.get(i).speed = speedFactor/(player.speed + speedFactor);
    enemies.get(i).draw();
  }
  
  
  if(!gameOver)
  {

    for (int i = 0; i < bulletCount; i += 1)
    {
      bullets.get(i).draw(); 
      for(int j = 0; j < enemies.size(); j++)
      {
        if(bullets.get(i).x > enemies.get(j).x-20 && bullets.get(i).y > enemies.get(j).y-26 && bullets.get(i).y < enemies.get(j).y + enemies.get(j).h/2+26 && bullets.get(i).x < enemies.get(j).x + enemies.get(j).w/2+20 )  
        { 
          enemies.get(j).life--;         
          
          if(enemies.get(j).life == 0)
          {
            enemies.remove(j);
            bullets.remove(i);
           
          }
    
          if(enemies.size() == 0)
          {
            currentLevel++;
            setup();
          }
          
        }
      }
  
  
    }
    for(int i = 0; i < enemies.size();i++)
    {
      if(player.x > enemies.get(i).x-40 && player.y > enemies.get(i).y-26 && player.x < enemies.get(i).x+ enemies.get(i).w+40 && player.y < enemies.get(i).y+enemies.get(i).h+6 && !gameOver)
      {        
          gameOver = true; //<>//
  
      }
    }
  }


}

void mouseReleased()
{
  if (mouseButton == LEFT) //<>//
  {
    bullets.get(bulletIndex).x = player.x;
    bullets.get(bulletIndex).y = player.y;
    bullets.get(bulletIndex).speed = 10;
    bullets.get(bulletIndex).directionX = player.directionX;
    bullets.get(bulletIndex).directionY = player.directionY;
    bulletIndex += 1;
    if (bulletIndex >= bulletCount)
    {
      bulletIndex = 0;
    }
  }
}
 
void keyPressed()
{

    if (key == 'w')
    {
      forwardKeyPressed = true;
    }
    if (key == 's')
    {
      backwardKeyPressed = true;
    }
    
    if(key == ' ' && gameOver)
    {
       currentLevel = 1;
       setup();
    }
}
 
void keyReleased()
{
    if (key == 'w')
    {
      forwardKeyPressed = false;
    }
    if (key == 's') 
    {
      backwardKeyPressed = false;
    }
}


class Hitman 
{
  float x = 0.0;
  float y = 0.0;
  float w = 30.0;
  float h = 30.0;
  float rotation = 0.0;
  float speed;
  float directionX;
  float directionY;
  float s = 2.0;
  PImage sprite;
  
  Hitman()
  {
    this.sprite = requestImage("hitman1_gun.png");
  }
  
  // function to point Hitman at a position
  void lookAt(float otherX, float otherY)
  {
    this.rotation = degrees(atan2(otherY-this.y, otherX-this.x));
    this.directionX = cos(radians(this.rotation));
    this.directionY = sin(radians(this.rotation));
  }
  
  void draw()
  {
    if (forwardKeyPressed)
    {
      this.x += this.directionX * this.speed;
      this.y += this.directionY * this.speed;
    }
    // subtract to go backward
    if (backwardKeyPressed)
    {
      this.x -= this.directionX * this.speed;
      this.y -= this.directionY * this.speed;
    }
  
    // apply transformations
    imageMode(CENTER);
    pushMatrix();
    translate(this.x, this.y);
    rotate(radians(this.rotation));
    scale(this.s);
    
    image(this.sprite, 0, 0, this.w, this.h);
    
    popMatrix();
    
    imageMode(CORNER);
  }  
}

class Enemy
{
  float x = 0.0;
  float y = 0.0;
  float w = 30.0;
  float h = 30.0;
  float rotation = 0.0;
  float speed;
  float directionX;
  float directionY;
  float s = 2.0;
  int life = 1;

  PImage img;

  Enemy(int level)
  {
    if(level%3 == 0)
        this.img = requestImage("zombie_hold.png");
    else if(level%3 == 1)
        this.img = requestImage("robot_hold.png");
    else if(level%3 == 2)
        this.img = requestImage("soldier_hold.png");  
  }
  
  void lookAt(float otherX, float otherY)
  {
    this.rotation = degrees(atan2(otherY-this.y, otherX-this.x));
    this.directionX = cos(radians(this.rotation));
    this.directionY = sin(radians(this.rotation));
  }
  void draw()
  {
    this.rotation = degrees(atan2(this.directionY, this.directionX));

    imageMode(CENTER);
    pushMatrix();
    translate(this.x, this.y);
    rotate(radians(this.rotation));
    scale(this.s);
   
    image(this.img, 0, 0, this.w, this.h);    
    popMatrix();
    
    imageMode(CORNER);
    this.x += this.directionX * this.speed;
    this.y += this.directionY * this.speed;
  }
  
  void kill()
  {
    
  }

    
  
}
// Bullet Class 
class Bullet
{
  float x = 0.0;
  float y = 0.0;
  float w = 20.0;
  float h = 20.0;
  float rotation = 0.0;
  float speed;
  float directionX;
  float directionY;
  float s = 1.0;
  PImage img;
  
  Bullet()
  {
    this.img = requestImage("Fireball.png");
  }
   
  void draw()
  {
    this.rotation = degrees(atan2(this.directionY, this.directionX));
    this.y += this.directionY * this.speed;
    this.x += this.directionX * this.speed;
    
    if (this.img.width > 0)
    {
      imageMode(CENTER);
      pushMatrix();
      translate(this.x, this.y);
      rotate(radians(this.rotation));
      scale(this.s);
      image(this.img, 30, 13, this.w, this.h);
      popMatrix();
      
      imageMode(CORNER);
    }
  }
}