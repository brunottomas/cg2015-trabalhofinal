####################
HIT-MAN
####################

Hit-Man é um endless shooter em terceira pessoa com perspectiva top-down

1.Objetivo
O Objetivo do jogo é acumular o máximo de pontos através das fases. Seus pontos aparecem no canto superior esquerdo da tela.

Cada fase consiste em derrotar os inimigos presentes na tela, atirando neles. A cada fase, surgem mais inimigos, cada vez mais rápidos dispostos a tudo para capturar o pobre atirador careca.

Cuidado! Caso algum inimigo consiga capturar o Hit-man é GAME OVER para você!

2.Controles

[w] - mover para frente
[s] - mover para trás
[mouse] - direcionar a visão do Hit-man
[left click] - atirar

3.Créditos

Hit-man foi desenvolvido por Bruno Tomas e Victor Cardoso usando Processing.